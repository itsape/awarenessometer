# 2.0.0
* subject's arm status is now inferred by the amount of time they have been exposed to an episode
* threshold for the exposure time is configurable with the optional `--min-seconds-exposed` flag and
  defaults to 60 seconds
* positional cli argument `no_vdu_employees` has been changed to be an optional flag
  `--potential-victims` and defaults to the number of subjects in the study

# 1.0.0
* 🚀 public release
