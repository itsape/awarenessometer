# AWARENESSOMETER
*The report generator for the [artifact-o-mat](https://gitlab.com/itsape/artifactomat).*

## USAGE
```
usage: awarenessometer [-h] [--potential-victims POTENTIAL_VICTIMS] [--min-seconds-exposed MIN_SECONDS_EXPOSED] [--author AUTHOR] {pre-post,labeled} data_path out_path

positional arguments:
  {pre-post,labeled}    The type of study you want to analyze, one of [pre-post, labeled].
  data_path             The path to the input files generated by the artifact-o-mat.
  out_path              The path where the report should be stored.

optional arguments:
  -h, --help            show this help message and exit
  --potential-victims POTENTIAL_VICTIMS
                        Number of employees with a visual display workstation, defaults to the number of subjects in the study.
  --min-seconds-exposed MIN_SECONDS_EXPOSED
                        Number of seconds a subject has to be exposed to an artifact to be included in the results. Default: 60.
  --author AUTHOR       Name of the author
```

## Expected files inside the `data_path` folder:
Depending on the evaluation different data is expected to be present in the `data_path`.
Two evaluation type are supported, *labled* and *pre/post*.

### Labeled study
- `intervention.csv`: output from `ape export`
- `report.csv`: output from `ape report series -f csv`
- `series.yml`: output from `ape series show`
- `img` directory: contains images of the artifacts


### Pre / Post study
- `intervention_pre.csv`: output from `ape export` or selfmade csv with the same format
- `intervention_post.csv`: output from `ape export` or selfmade csv with the same format
- `report_pre.csv`: output from `ape report series -f csv` for the first series
- `report_post.csv`: output from `ape report series -f csv` for the second series
- `series_pre.yml`: output from `ape series show` for the first series
- `series_post.yml`: output from `ape series show` for the second series
- `pseudonym_links.yml`: output from `ape pseudonymize group1 group2`
- `img` directory: contains images of the artifacts


### `img` directory
Inside the `img` directory you can store images for the artifacts.
An image for an artifact has to start with the corresponding `recipe_id`.

Example folder structure for a pre / post study where `data_path` is `./test_data`:
```
.
└── test_data
    ├── img
    │   ├── artifact1_1.jpg
    │   ├── artifact1_2.png
    │   └── artifact2.jpg
    ├── intervention_pre.csv
    ├── intervention_post.csv
    ├── pseudonym_links.csv
    ├── report_post.yml
    ├── report_pre.yml
    ├── series_post.yml
    └── series_pre.yml

```
