numpy==1.22.2
pandas==1.4.2
pyaml==19.12.0
Mako==1.1.1
mpmath==1.1.0
scipy==1.8.1
matplotlib==3.5.2
