import importlib
import os

import yaml
import pytest
import pandas as pd
import numpy as np
from scipy.stats import pearsonr

import awarenessometer.labeled_study


_DATA_PATH = os.path.join(os.path.dirname(__file__), 'test_data')
with open(os.path.join(_DATA_PATH, 'series_data.yml'), 'r') as stream:
    SERIES_DATA= yaml.safe_load(stream)
SUBJECTS_DF = pd.read_csv(os.path.join(_DATA_PATH, 'labeled_subjects.csv'))
REPORT = pd.read_csv(os.path.join(_DATA_PATH, 'extended_report.csv'))
IMG_PATH = os.path.abspath(os.path.join(_DATA_PATH, 'img'))
GOOD_EVAL_MATRIX = np.loadtxt(os.path.abspath(os.path.join(_DATA_PATH, 'labeled_good_eval_matrix.csv')))
POSITIVE_ACTION_MATRIX = np.loadtxt(os.path.abspath(os.path.join(_DATA_PATH, 'labeled_positive_action_matrix.csv')))
STUDY_INFO = {
    'series_data': SERIES_DATA,
    'subject_df': SUBJECTS_DF,
    'report': REPORT,
    'matrices': {
        'good_evals': GOOD_EVAL_MATRIX,
        'positive_actions': POSITIVE_ACTION_MATRIX
    }
}


def img_path(filename):
    return os.path.join('img', filename)


def test_create_tokens():
    series_info = {
        'title': 'LabeledTestSeries',
        'start': '2020-03-06 12:12:51',
        'end': '2020-03-06 12:22:51',
        '#participants': 10,
        '#artifacts': 2
    }
    artifacts = [
        {'recipe_id': 'LabeledRecipe1', 'images': [img_path('LabeledRecipe1_1.jpg'), img_path('LabeledRecipe1_2.png')]},
        {'recipe_id': 'LabeledRecipe2', 'images': []}
    ]
    participation_stats = {
        '#artifacts': '1.70 0.67 0 2',
        '#schooled': '5 (50.00%)',
        '#armed': '9 (90.00%)',
        '#missed': '1 (10.00%)'
    }
    reaction_stats = {
        'p_nopos': '47.06%',
        'p_pos_neg': '35.29%',
        'p_pos_noneg': '17.65%',
        'p_nopos_neg': '23.53%',
        'p_nopos_noneg': '23.53%',
        'iv_p_nopos': '40.00%',
        'noiv_p_nopos': '57.14%',
        'iv_p_neg': '40.00%',
        'noiv_p_neg': '85.71%',
        'iv_p_nopos_neg': '10.00%',
        'noiv_p_nopos_neg': '42.86%',
        'iv_citsa': '1.0000',
        'noiv_citsa': '0.9999',
        'citsa_increase': '0.01%',
        'iv_iitsa': '0.9000',
        'noiv_iitsa': '0.5714',
        'iitsa_increase': '57.50%',
        'good_evals_mean': '0.78',
        'good_evals_std': '0.36',
        'good_evals_min': '0.00',
        'good_evals_max': '1.00',
        'bad_evals_mean': '0.22',
        'bad_evals_std': '0.36',
        'bad_evals_min': '0.00',
        'bad_evals_max': '1.00',
        'good_evals_iv_mean': '0.90',
        'good_evals_iv_std': '0.22',
        'good_evals_iv_min': '0.50',
        'good_evals_iv_max': '1.00',
        'bad_evals_iv_mean': '0.10',
        'bad_evals_iv_std': '0.22',
        'bad_evals_iv_min': '0.00',
        'bad_evals_iv_max': '0.50',
        'good_evals_noiv_mean': '0.62',
        'good_evals_noiv_std': '0.48',
        'good_evals_noiv_min': '0.00',
        'good_evals_noiv_max': '1.00',
        'bad_evals_noiv_mean': '0.38',
        'bad_evals_noiv_std': '0.48',
        'bad_evals_noiv_min': '0.00',
        'bad_evals_noiv_max': '1.00'
    }
    cronbachs_alpha = {
        'good_eval': '0.33',
        'positive_action': '0.01'
    }
    expected_tokens = {
        'series_info': series_info,
        'participation_stats': participation_stats,
        'reaction_stats': reaction_stats,
        'artifacts': artifacts,
        'cronbachs_alpha': cronbachs_alpha
    }
    study = awarenessometer.labeled_study.LabeledStudy(10000, STUDY_INFO, IMG_PATH)
    tokens = study.create_tokens()

    assert tokens == expected_tokens


class TestPlotCreation:
    def test_create_good_eval_correlations(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.labeled_study.Plotter, 'distplot')
        artifact1_values = [13/17, 1, 13/17, 0, 0, 1, 1, 1, 1, 1]
        artifact2_values = [13/17, 1, 1, 0, 1, 1, 1, 1, 0, 1]
        correlations = [pearsonr(artifact1_values, artifact2_values)[0]]
        plt_path = os.path.join(IMG_PATH, 'fig_good_eval_correlation_distplot.pdf')

        study = awarenessometer.labeled_study.LabeledStudy(10000, STUDY_INFO, IMG_PATH)
        study._plot_good_eval_correlations()

        mock_plotter.assert_called_with(correlations, plt_path)

    def test_create_positive_action_correlations(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.labeled_study.Plotter, 'distplot')
        artifact1_values = [9/17, 0, 9/17, 0, 0, 0, 1, 1, 1, 1]
        artifact2_values = [9/17, 0, 1, 0, 1, 1, 1, 1, 0, 0]
        correlations = [pearsonr(artifact1_values, artifact2_values)[0]]
        plt_path = os.path.join(IMG_PATH, 'fig_positive_action_correlation_distplot.pdf')

        study = awarenessometer.labeled_study.LabeledStudy(10000, STUDY_INFO, IMG_PATH)
        study._plot_positive_action_correlations()

        mock_plotter.assert_called_with(correlations, plt_path)

    def test_create_reactions_plot(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.labeled_study.Plotter, 'reactions_by_artifact_relative')

        artifacts = ['LabeledRecipe1', 'LabeledRecipe2']
        no_reactions = {
            'good': [-(4 / 8), -(5 / 9)],
            'bad': [4 / 8, 6 / 9],
            'good_and_bad': [2 / 8, 4 / 9]
        }
        plt_path = os.path.join(IMG_PATH, 'fig_reactions_by_artifact_relative.pdf')

        study = awarenessometer.labeled_study.LabeledStudy(10000, STUDY_INFO, IMG_PATH)
        study._plot_reactions_by_artifact_relative()

        mock_plotter.assert_called_with(artifacts, no_reactions, 0, plt_path)

    def test_create_kde(self, mocker):
        mock_plotter = mocker.patch.object(awarenessometer.labeled_study.Plotter, 'iitsa_kde')

        iitsas = [pd.Series([2, 2, 1, 2, 2]), pd.Series([1, 0, 2, 1])]
        iitsa_iv = pd.Series([2 / 2, 2 / 2,  1 / 2, 2 / 2, 2 / 2])
        iitsa_noiv = pd.Series([1 / 1, 0 / 2, 2 / 2, 1 / 2])
        plt_path = os.path.join(IMG_PATH, 'fig_iitsa_kde.pdf')

        study = awarenessometer.labeled_study.LabeledStudy(10000, STUDY_INFO, IMG_PATH)
        study._plot_iitsa_kernel_density()

        assert len(mock_plotter.call_args[0][0]) == 2
        assert mock_plotter.call_args[0][0][0].reset_index(drop=True).equals(iitsas[0])
        assert mock_plotter.call_args[0][0][1].reset_index(drop=True).equals(iitsas[1])
        assert mock_plotter.call_args[0][1].reset_index(drop=True).equals(iitsa_iv)
        assert mock_plotter.call_args[0][2].reset_index(drop=True).equals(iitsa_noiv)
        assert mock_plotter.call_args[0][3] == plt_path
        assert mock_plotter.call_args[0][4] == 0
