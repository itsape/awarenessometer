import yaml
from os import path

import pandas as pd
import numpy as np

from awarenessometer.data_preprocessor import DataPreprocessor

TEST_DATA_PATH = path.join(path.dirname(__file__), 'test_data')

def _load_yaml(name):
    with open(path.join(TEST_DATA_PATH, name), 'r') as stream:
        data = yaml.safe_load(stream)
    return data

def _load_csv(name):
    return pd.read_csv(path.join(TEST_DATA_PATH, name), keep_default_na=False, na_values=[])

class TestDataPreprocessor:
    def test_compute_pre_post_subjects_df(self):
        series_pre = _load_csv('series_pre_df.csv')
        series_post = _load_csv('series_post_df.csv')
        iv_participants_pre = _load_csv('intervention_pre.csv')
        iv_participants_post = _load_csv('intervention_post.csv')
        report_pre = _load_csv('report_pre.csv')
        report_post = _load_csv('report_post.csv')
        pseudonym_links = _load_yaml('pseudonym_links.yml')

        df = DataPreprocessor.compute_pre_post_subject_df(series_pre,
                                                          series_post,
                                                          iv_participants_pre,
                                                          iv_participants_post,
                                                          report_pre,
                                                          report_post,
                                                          pseudonym_links)
        expected_df = _load_csv('pre_post_subjects.csv')
        assert df.equals(expected_df)

    def test_compute_labeled_subjects_df(self):
        series = _load_csv('series_df.csv')
        iv_participants = _load_csv('intervention.csv')
        report = _load_csv('report.csv')

        df = DataPreprocessor.compute_labeled_subject_df(series,
                                                         iv_participants,
                                                         report)
        expected_df = _load_csv('labeled_subjects.csv')
        assert df.equals(expected_df)


    def test_extract_series_data(self):
        series = _load_yaml('series.yml')

        data = DataPreprocessor.extract_series_data(series)

        expected_data = _load_yaml('series_data.yml')
        assert data == expected_data


    def test_series_to_df(self):
        series = _load_yaml('series.yml')
        expected_df = _load_csv('series_df.csv')
        min_exposure = 60

        df60 = DataPreprocessor.series_to_df(series, min_exposure)
        assert df60.equals(expected_df)

        min_exposure = 120
        df120 = DataPreprocessor.series_to_df(series, min_exposure)
        assert not df120.equals(expected_df)

        # There are two subjects with an exposure of 60 seconds
        armed60 = df60['armed'].sum()
        armed120 = df120['armed'].sum()
        assert (armed60 - armed120) == 2


    def test_extend_report(self):
        report = _load_csv('report.csv')
        series_df = _load_csv('series_df.csv')
        expected_df = _load_csv('extended_report.csv')

        df = DataPreprocessor.extend_report(report, series_df)
        assert df.equals(expected_df)

    def test_get_positive_actions(self):
        expected_result = np.array([[np.nan, np.nan, np.nan, np.nan],
                                    [0., 0., np.nan, np.nan],
                                    [np.nan, 1., np.nan, np.nan],
                                    [0., 0., np.nan, np.nan],
                                    [0., 1., np.nan, np.nan],
                                    [0., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 0., np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, 1.],
                                    [np.nan, np.nan, 0., 0.],
                                    [np.nan, np.nan, 1., 1.],
                                    [np.nan, np.nan, 0., 0.],
                                    [np.nan, np.nan, 0., 1.],
                                    [np.nan, np.nan, 1., 0.],
                                    [np.nan, np.nan, 0., 0.]])
        report_pre = _load_csv('extended_report_pre.csv')
        report_post = _load_csv('extended_report_post.csv')
        reports = [report_pre, report_post]
        artifacts = ['PreRecipe1', 'PreRecipe2', 'PostRecipe1', 'PostRecipe2']
        iv_participants_pre = _load_csv('intervention_pre.csv')
        iv_participants_post = _load_csv('intervention_post.csv')
        iv_participants = [iv_participants_pre, iv_participants_post]
        positive_action_matrix = DataPreprocessor.get_positive_actions(reports, artifacts, iv_participants)
        np.testing.assert_equal(positive_action_matrix, expected_result)

    def test_get_good_evaluations(self):
        expected_result = np.array([[np.nan, np.nan, np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [np.nan, 1., np.nan, np.nan],
                                    [0., 0., np.nan, np.nan],
                                    [0., 1., np.nan, np.nan],
                                    [0., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [1., 1., np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, np.nan],
                                    [np.nan, np.nan, np.nan, 1.],
                                    [np.nan, np.nan, 0., 0.],
                                    [np.nan, np.nan, 1., 1.],
                                    [np.nan, np.nan, 0., 1.],
                                    [np.nan, np.nan, 0., 1.],
                                    [np.nan, np.nan, 1., 1.],
                                    [np.nan, np.nan, 1., 1.]])
        report_pre = _load_csv('extended_report_pre.csv')
        report_post = _load_csv('extended_report_post.csv')
        reports = [report_pre, report_post]
        artifacts = ['PreRecipe1', 'PreRecipe2', 'PostRecipe1', 'PostRecipe2']
        iv_participants_pre = _load_csv('intervention_pre.csv')
        iv_participants_post = _load_csv('intervention_post.csv')
        iv_participants = [iv_participants_pre, iv_participants_post]
        good_eval_matrix = DataPreprocessor.get_good_evaluations(reports, artifacts, iv_participants)
        np.testing.assert_equal(good_eval_matrix, expected_result)
