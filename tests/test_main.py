from argparse import ArgumentError
import os
from os import path

import pandas as pd
import numpy as np
import pytest
import yaml

import awarenessometer.__main__ as main


TEST_DATA_PATH = path.join(path.dirname(__file__), 'test_data')
IMAGE_PATH = path.join(path.abspath('.'), 'img')


def _load_yml(name):
    with open(path.join(TEST_DATA_PATH, name), 'r') as stream:
        data = yaml.safe_load(stream)
    return data

def _load_csv(name):
    return pd.read_csv(path.join(TEST_DATA_PATH, name))


class NameSpaceMock:
    def __init__(self, study_type, out_path='report.md'):
        self.study_type = study_type
        self.data_path = TEST_DATA_PATH
        self.out_path = out_path
        self.potential_victims = 10000
        self.min_seconds_exposed = 60
        self.author = 'Super Author'


class TestImageCopying:
    def test_copy_img_dir(self, tmpdir, mocker):
        out_path = path.join(tmpdir, 'report.md')
        os.makedirs(path.join(tmpdir, 'img'))
        mock_namespace = NameSpaceMock('labeled', out_path)

        main._copy_img_dir(mock_namespace)

        images_in = os.listdir(path.join(TEST_DATA_PATH, 'img'))
        images_out = os.listdir(path.join(tmpdir, 'img'))
        assert set(images_in) == set(images_out)


class TestStudyCreation:
    def test_create_labeled_study(self, mocker):
        mock_dp_cdf = mocker.patch.object(main.DataPreprocessor,
                                          'compute_labeled_subject_df')
        mock_dp_esd = mocker.patch.object(main.DataPreprocessor,
                                          'extract_series_data')
        mock_dp_std = mocker.patch.object(main.DataPreprocessor,
                                          'series_to_df')
        mock_dp_erp = mocker.patch.object(main.DataPreprocessor,
                                          'extend_report')
        mock_dp_gge = mocker.patch.object(main.DataPreprocessor,
                                          'get_good_evaluations')
        mock_dp_gpa = mocker.patch.object(main.DataPreprocessor,
                                          'get_positive_actions')
        mock_dp_cdf.return_value = pd.DataFrame()
        mock_dp_esd.return_value = {'artifacts': ['LabeledRecipe1', 'LabeledRecipe2']}
        mock_dp_std.return_value = pd.DataFrame()
        mock_dp_erp.return_value = pd.DataFrame()
        mock_dp_gge.return_value = np.array([])
        mock_dp_gpa.return_value = np.array([])

        series = _load_yml('series.yml')
        series_df = _load_csv('series_df.csv')
        report = _load_csv('report.csv')
        iv_participants = _load_csv('intervention.csv')

        study = main._create_labeled_study(TEST_DATA_PATH, IMAGE_PATH, 10000, 60)

        assert mock_dp_erp.call_args[0][0].equals(report)

        mock_dp_std.assert_called_with(series, 60)

        # mock_dp_cdf.assert_called_with(series, iv_participants)
        # pandas frames are not supported in Mock.assert_called_with, so we
        # need to test each argument indicidually
        assert mock_dp_cdf.call_args[0][0].equals(pd.DataFrame())
        assert mock_dp_cdf.call_args[0][1].equals(iv_participants)
        assert mock_dp_cdf.call_args[0][2].equals(report)

        assert mock_dp_gge.call_args[0][0][0].equals(pd.DataFrame())
        assert set(mock_dp_gge.call_args[0][1]) == {'LabeledRecipe1', 'LabeledRecipe2'}
        assert mock_dp_gge.call_args[0][2][0].equals(iv_participants)

        assert mock_dp_gpa.call_args[0][0][0].equals(pd.DataFrame())
        assert set(mock_dp_gpa.call_args[0][1]) == {'LabeledRecipe1', 'LabeledRecipe2'}
        assert mock_dp_gpa.call_args[0][2][0].equals(iv_participants)
        mock_dp_esd.assert_called_with(series)

        assert study.series_data == {'artifacts': ['LabeledRecipe1', 'LabeledRecipe2']}
        assert study.subjects.equals(pd.DataFrame())
        assert study.img_dir == IMAGE_PATH
        assert study.report.equals(pd.DataFrame())
        assert study.employees_with_vdu == 10000
        np.testing.assert_equal(study.matrices['good_evals'], np.array([]))
        np.testing.assert_equal(study.matrices['positive_actions'], np.array([]))

    def test_create_labeled_study_infers_no_vdu_employees(self, mocker):
        mock_dp_cdf = mocker.patch.object(main.DataPreprocessor,
                                          'compute_labeled_subject_df')
        mock_dp_esd = mocker.patch.object(main.DataPreprocessor,
                                          'extract_series_data')
        mock_dp_std = mocker.patch.object(main.DataPreprocessor,
                                          'series_to_df')
        mock_dp_erp = mocker.patch.object(main.DataPreprocessor,
                                          'extend_report')
        mock_dp_gge = mocker.patch.object(main.DataPreprocessor,
                                          'get_good_evaluations')
        mock_dp_gpa = mocker.patch.object(main.DataPreprocessor,
                                          'get_positive_actions')
        mock_dp_cdf.return_value = pd.DataFrame()
        mock_dp_esd.return_value = {
            'artifacts': ['LabeledRecipe1', 'LabeledRecipe2'],
            'participants': ['alpha.csv/1', 'alpha.csv/2', 'alpha.csv/3', 'alpha.csv/4']
        }
        mock_dp_std.return_value = pd.DataFrame()
        mock_dp_erp.return_value = pd.DataFrame()
        mock_dp_gge.return_value = np.array([])
        mock_dp_gpa.return_value = np.array([])

        study = main._create_labeled_study(TEST_DATA_PATH, IMAGE_PATH, None, 60)

        assert study.employees_with_vdu == 4

    def test_create_pre_post_study(self, mocker):
        mock_dp_cdf = mocker.patch.object(main.DataPreprocessor,
                                          'compute_pre_post_subject_df')
        mock_dp_esd = mocker.patch.object(main.DataPreprocessor,
                                          'extract_series_data')
        mock_dp_std = mocker.patch.object(main.DataPreprocessor,
                                          'series_to_df')
        mock_dp_erp = mocker.patch.object(main.DataPreprocessor,
                                          'extend_report')
        mock_dp_gge = mocker.patch.object(main.DataPreprocessor,
                                          'get_good_evaluations')
        mock_dp_gpa = mocker.patch.object(main.DataPreprocessor,
                                          'get_positive_actions')
        mock_dp_cdf.return_value = pd.DataFrame()
        mock_dp_esd.side_effect = [{'artifacts': ['PreRecipe1', 'PreRecipe2']}, {'artifacts': ['PostRecipe1', 'PostRecipe2']}]
        mock_dp_std.return_value = pd.DataFrame()
        mock_dp_erp.return_value = pd.DataFrame()
        mock_dp_gge.return_value = np.array([])
        mock_dp_gpa.return_value = np.array([])

        series_pre = _load_yml('series_pre.yml')
        series_post = _load_yml('series_post.yml')
        series_pre_df = _load_csv('series_pre_df.csv')
        series_post_df = _load_csv('series_post_df.csv')
        report_pre = _load_csv('report_pre.csv')
        report_post = _load_csv('report_post.csv')
        pseudonym_links = _load_yml('pseudonym_links.yml')
        iv_participants_pre = _load_csv('intervention_pre.csv')
        iv_participants_post = _load_csv('intervention_post.csv')
        dp_std_series_calls = [mocker.call(series_pre, 60), mocker.call(series_post, 60)]
        dp_esd_series_calls = [mocker.call(series_pre), mocker.call(series_post)]

        study = main._create_pre_post_study(TEST_DATA_PATH, IMAGE_PATH, 10000, 60)

        mock_dp_std.assert_has_calls(dp_std_series_calls)

        dp_erp_calls = mock_dp_erp.call_args_list
        assert dp_erp_calls[0][0][0].equals(report_pre)
        assert dp_erp_calls[1][0][0].equals(report_post)

        assert mock_dp_cdf.call_args[0][0].equals(pd.DataFrame())
        assert mock_dp_cdf.call_args[0][1].equals(pd.DataFrame())
        assert mock_dp_cdf.call_args[0][2].equals(iv_participants_pre)
        assert mock_dp_cdf.call_args[0][3].equals(iv_participants_post)
        assert mock_dp_cdf.call_args[0][4].equals(report_pre)
        assert mock_dp_cdf.call_args[0][5].equals(report_post)
        assert mock_dp_cdf.call_args[0][6] == pseudonym_links

        assert mock_dp_gge.call_args[0][0][0].equals(pd.DataFrame())
        assert mock_dp_gge.call_args[0][0][1].equals(pd.DataFrame())
        assert set(mock_dp_gge.call_args[0][1]) == {'PreRecipe1', 'PreRecipe2', 'PostRecipe1', 'PostRecipe2'}
        assert mock_dp_gge.call_args[0][2][0].equals(iv_participants_pre)
        assert mock_dp_gge.call_args[0][2][1].equals(iv_participants_post)

        assert mock_dp_gpa.call_args[0][0][0].equals(pd.DataFrame())
        assert mock_dp_gpa.call_args[0][0][1].equals(pd.DataFrame())
        assert set(mock_dp_gpa.call_args[0][1]) == {'PreRecipe1', 'PreRecipe2', 'PostRecipe1', 'PostRecipe2'}
        assert mock_dp_gpa.call_args[0][2][0].equals(iv_participants_pre)
        assert mock_dp_gpa.call_args[0][2][1].equals(iv_participants_post)

        mock_dp_esd.assert_has_calls(dp_esd_series_calls)

        assert study.series_pre_data == {'artifacts': ['PreRecipe1', 'PreRecipe2']}
        assert study.series_post_data == {'artifacts': ['PostRecipe1', 'PostRecipe2']}
        assert study.subjects.equals(pd.DataFrame())
        assert study.img_dir == IMAGE_PATH
        assert study.reports['pre'].equals(pd.DataFrame())
        assert study.reports['post'].equals(pd.DataFrame())
        np.testing.assert_equal(study.matrices['good_evals'], np.array([]))
        np.testing.assert_equal(study.matrices['positive_actions'], np.array([]))
        assert study.employees_with_vdu == 10000

    def test_create_pre_post_study_infers_no_vdu_employees(self, mocker):
        mock_dp_cdf = mocker.patch.object(main.DataPreprocessor,
                                          'compute_pre_post_subject_df')
        mock_dp_esd = mocker.patch.object(main.DataPreprocessor,
                                          'extract_series_data')
        mock_dp_std = mocker.patch.object(main.DataPreprocessor,
                                          'series_to_df')
        mock_dp_erp = mocker.patch.object(main.DataPreprocessor,
                                          'extend_report')
        mock_dp_gge = mocker.patch.object(main.DataPreprocessor,
                                          'get_good_evaluations')
        mock_dp_gpa = mocker.patch.object(main.DataPreprocessor,
                                          'get_positive_actions')
        mock_dp_cpl = mocker.patch.object(main.DataPreprocessor,
                                          'complete_pseudonym_links')
        mock_dp_cdf.return_value = pd.DataFrame()
        mock_dp_esd.side_effect = [
            {
                'artifacts': ['PreRecipe1', 'PreRecipe2'],
                'participants': ['alpha.csv/1', 'alpha.csv/2', 'alpha.csv/3']
            },
            {
                'artifacts': ['PostRecipe1', 'PostRecipe2'],
                'participants': ['beta.csv/1', 'beta.csv/2', 'beta.csv/3']
            }
        ]
        mock_dp_std.return_value = pd.DataFrame()
        mock_dp_erp.return_value = pd.DataFrame()
        mock_dp_gge.return_value = np.array([])
        mock_dp_gpa.return_value = np.array([])
        mock_dp_cpl.return_value = {'alpha.csv/1': 'beta.csv/1', 'alpha.csv/2': 'beta.csv/3'}

        pseudonym_links = _load_yml('pseudonym_links.yml')

        study = main._create_pre_post_study(TEST_DATA_PATH, IMAGE_PATH, None, 60)

        mock_dp_cpl.assert_called_with(pseudonym_links)
        assert study.employees_with_vdu == 4


class TestArgParse:
    def test_argparse_parsing(self):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md', '--potential-victims', '10000',
                     '--min-seconds-exposed', '70', '--author', 'SuperAuthor']
        args = main._parse_args(arguments)
        assert args.potential_victims == 10000
        assert args.min_seconds_exposed == 70
        assert args.study_type == 'pre-post'
        assert args.data_path == '/tmp/test'
        assert args.out_path == '/tmp/test/report.md'
        assert args.author == 'SuperAuthor'

    def test_min_seconds_exposed_has_default(self):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md', '--author', 'Super Author',
                     '--potential-victims', '10000']
        args = main._parse_args(arguments)
        assert args.potential_victims == 10000
        assert args.min_seconds_exposed == 60
        assert args.study_type == 'pre-post'
        assert args.data_path == '/tmp/test'
        assert args.out_path == '/tmp/test/report.md'
        assert args.author == 'Super Author'

    def test_author_is_optional(self):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md', '--potential-victims', '10000']
        args = main._parse_args(arguments)
        assert args.potential_victims == 10000
        assert args.min_seconds_exposed == 60
        assert args.study_type == 'pre-post'
        assert args.data_path == '/tmp/test'
        assert args.out_path == '/tmp/test/report.md'
        assert args.author is None

    def test_potential_victims_is_optional(self):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md']
        args = main._parse_args(arguments)
        assert args.potential_victims is None
        assert args.min_seconds_exposed == 60
        assert args.study_type == 'pre-post'
        assert args.data_path == '/tmp/test'
        assert args.out_path == '/tmp/test/report.md'
        assert args.author is None

    def test_argparse_study_types(self):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md']
        main._parse_args(arguments)
        arguments = ['labeled', '/tmp/test', '/tmp/test/report.md']
        main._parse_args(arguments)

    def test_argparse_invalid_study_type(self, capsys):
        arguments = ['pre-post2', '/tmp/test', '/tmp/test/report.md']
        with pytest.raises(SystemExit) as raised:
            main._parse_args(arguments)
        output = capsys.readouterr()
        expected = ("error: argument study_type: invalid choice: 'pre-post2' "
                    "(choose from 'pre-post', 'labeled')")
        assert expected in output.err
        assert raised.type == SystemExit

    def test_argparse_missing_positional_argument(self, capsys):
        arguments = ['pre-post', '/tmp/test']
        with pytest.raises(SystemExit) as raised:
            main._parse_args(arguments)
        output = capsys.readouterr()
        expected = 'the following arguments are required: out_path'
        assert expected in output.err
        assert raised.type == SystemExit

    def test_argparse_additional_positional_argument(self, capsys):
        arguments = ['pre-post', '/tmp/test', '/tmp/test/report.md', 'toomuch']
        with pytest.raises(SystemExit) as raised:
            main._parse_args(arguments)
        output = capsys.readouterr()
        expected = 'error: unrecognized arguments: toomuch'
        assert expected in output.err
        assert raised.type == SystemExit

class TestMain():
    def test_create_labeled_report(self, mocker):
        mock_rc = mocker.patch.object(main, 'ReportCreator')
        mock_csr = mocker.patch.object(main.ReportCreator,
                                       'create_study_report')
        mock_cid = mocker.patch.object(main, '_copy_img_dir')
        mock_namespace = NameSpaceMock('labeled')
        mock_rc.return_value = mock_rc

        mock_parse_args = mocker.patch.object(main, '_parse_args')
        mock_parse_args.return_value = mock_namespace

        mock_create_study = mocker.patch.object(main, '_create_labeled_study')
        mock_create_study.return_value = 'study'

        main.create_report()

        mock_rc.assert_called_with('report.md')
        mock_csr.assert_called_with('study', 'Super Author')
        mock_cid.assert_called_with(mock_namespace)

        mock_create_study.assert_called_with(TEST_DATA_PATH, IMAGE_PATH, 10000, 60)


    def test_create_pre_post_report(self, mocker):
        mock_rc = mocker.patch.object(main, 'ReportCreator')
        mock_csr = mocker.patch.object(main.ReportCreator,
                                       'create_study_report')
        mock_cid = mocker.patch.object(main, '_copy_img_dir')
        mock_namespace = NameSpaceMock('pre-post')
        mock_rc.return_value = mock_rc

        mock_parse_args = mocker.patch.object(main, '_parse_args')
        mock_parse_args.return_value = mock_namespace

        mock_create_study = mocker.patch.object(main, '_create_pre_post_study')
        mock_create_study.return_value = 'study'

        main.create_report()

        mock_rc.assert_called_with('report.md')
        mock_csr.assert_called_with('study', 'Super Author')
        mock_cid.assert_called_with(mock_namespace)

        mock_create_study.assert_called_with(TEST_DATA_PATH, IMAGE_PATH, 10000, 60)
