from subprocess import run

def test_entrypoint_exists():
    proc = run('awareness-o-meter', capture_output=True)
    err = proc.stderr.decode('ascii')
    assert 'awareness-o-meter: error: the following arguments are required:' in err
