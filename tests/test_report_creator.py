import os
import importlib
from datetime import date

import pytest

import awarenessometer.report_creator


class MockStudy:
    def __init__(self, type_):
        self.type = type_
        self.create_plots_called = False

    def create_tokens(self):
        tokens = {
            'subjects': ['alpha.csv/1', 'alpha.csv/2'],
            'artifacts_pre': ['pre1', 'pre2'],
            'artifacts_post': ['post1', 'post2']
        }
        return tokens

    def create_plots(self):
        self.create_plots_called = True


class TestTemplateTypeChoice:
    @classmethod
    def setup_class(cls):
        cls.old_lang = os.environ['LANG']
        os.environ['LANG'] = 'en_US.UTF-8'
        importlib.reload(awarenessometer.report_creator)

    @classmethod
    def teardown_class(cls):
        os.environ['LANG'] = cls.old_lang

    def test_pre_post_template(self, tmpdir, mocker):
        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('pre-post')

        expected_template = 'pre_post_template_en_US.md'

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[0] == expected_template

    def test_labeled_template(self, tmpdir, mocker):
        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('labeled')

        expected_template = 'labeled_template_en_US.md'

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[0] == expected_template

class TestTemplateLangChoice:
    @classmethod
    def setup_class(cls):
        # backup original language
        cls.old_lang = os.environ['LANG']

    @classmethod
    def teardown_class(cls):
        os.environ['LANG'] = cls.old_lang

    def test_english_template(self, tmpdir, mocker):
        os.environ['LANG'] = 'en_US.UTF-8'
        importlib.reload(awarenessometer.report_creator)

        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('labeled')

        expected_template = 'labeled_template_en_US.md'

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[0] == expected_template

    def test_german_template(self, tmpdir, mocker):
        os.environ['LANG'] = 'de_DE.UTF-8'
        importlib.reload(awarenessometer.report_creator)

        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('labeled')

        expected_template = 'labeled_template_de_DE.md'

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[0] == expected_template

    def test_unknown_template(self, tmpdir, mocker):
        os.environ['LANG'] = 'fr_FR.UTF-8'
        importlib.reload(awarenessometer.report_creator)

        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('labeled')

        expected_template = 'labeled_template_en_US.md'

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[0] == expected_template


class TestReportCreation:
    @classmethod
    def setup_class(cls):
        cls.old_lang = os.environ['LANG']
        os.environ['LANG'] = 'en_US.UTF-8'
        importlib.reload(awarenessometer.report_creator)

    @classmethod
    def teardown_class(cls):
        os.environ['LANG'] = cls.old_lang

    def test_create_report(self, tmpdir, mocker):
        mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
        study = MockStudy('pre-post')

        expected_tokens = {**study.create_tokens(), 'author': 'Super Author', 'date': date.today().isoformat()}
        expected_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../awarenessometer/templates'))

        report_path = os.path.join(tmpdir, 'report.md')
        rc = awarenessometer.report_creator.ReportCreator(report_path)
        rc.create_study_report(study, 'Super Author')

        args, kwargs = mock_render.call_args
        assert args[1] == expected_path
        assert args[2] == expected_tokens
        assert study.create_plots_called == True


def test_writes_to_out_file(tmpdir, mocker):
    mock_render = mocker.patch('awarenessometer.report_creator.ReportCreator._render_report')
    expected_content = 'This should go\nInto the file\n'
    mock_render.return_value = expected_content
    study = MockStudy('pre-post')

    report_path = os.path.join(tmpdir, 'report.md')
    rc = awarenessometer.report_creator.ReportCreator(report_path)
    rc.create_study_report(study, 'Super Author')

    assert os.path.isfile(report_path)
    with open(report_path, 'r') as stream:
        content = stream.read()
    assert content == expected_content
