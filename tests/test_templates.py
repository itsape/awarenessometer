import pytest
from os import path

from mako.template import Template
from mako.lookup import TemplateLookup

TEMPLATE_PATH = path.join(path.dirname(__file__), '../awarenessometer/templates')
LABELED_NAME_DE = 'labeled_template_de_DE.md'
PRE_POST_NAME_DE = 'pre_post_template_de_DE.md'
LABELED_NAME_EN = 'labeled_template_en_US.md'
PRE_POST_NAME_EN = 'pre_post_template_en_US.md'


class TestGerman:
    def test_labeled_template(self):
        expected_output = r"""
---
title: 'Report der Serie "test_title"'
author: 'Super Author'
mainfont: TeX Gyre Pagella
papersize: a4
date: 1970-01-01
lang: de-DE
---

# Studiendesign

In der Studie wurden **#participants Teilnehmern**
zwischen dem **start** und dem **end**
nacheinander jeweils **#artifacts Artefakte** zugespielt
und die relevanten Aktionen im bezug auf das jeweilige Artefakt aufgezeichnet.
Ein Teil der Teilnehmer hat vorher an einer Intervention teilgenommen.

# Teilnahme

- Anzahl der Artefakte (avg, std, min, max): **1.70 0.67 0 2**
- Anzahl der Interventionsteilnehmer (%): **5 (50.00%)**
- Anzahl der gearmten Teilnehmer (%): **9 (90.00%)**
- Anzahl der nicht-gearmten Teilnehmer (%): **1 (10.00%)**

# Reaktionen

Alle Werte der Tabellen 1 bis 3 geben die durchschnittliche relative Häufigkeit
für die folgenden Ereignisse an:  
**-pos**: keine positive Reaktion  
**neg**: negative Reaktion  
**pos ∩ neg**: sowohl positive, als auch negative Reaktion  
**pos ∩ -neg**: positive Reaktion, aber keine negative Reaktion  
**-pos ∩ neg**: keine positive Reaktion, aber negative Reaktion  
**-pos ∩ -neg**: weder positive, noch negative Reaktion  

| -pos | pos ∩ neg | pos ∩ -neg | -pos ∩ neg | -pos ∩ -neg |
| ---: | --------: | ---------: | ---------: | ----------: |
| 41.18% | 41.18% | 17.65% | 17.65% | 23.53% |
: Reaktionen aller Teilnehmer.

| -pos | neg | -pos ∩ neg |
| ---: | --: | ---------: |
| 30.00% | 40.00% | 0.00% |
: Reaktionen von Interventionsteilnehmern.

| -pos | neg | -pos ∩ neg |
| ---: | --: | ---------: |
| 57.14% | 85.71% | 42.86% |
: Reaktionen von Nicht-Interventionsteilnehmern.

![Nutzer Aktionen als Reaktion auf Artefakte.](img/fig_reactions_by_artifact_relative.pdf)

![Kerndichteschätzung der Korrelationen zwischen positiven Reaktionen verschiedener Artefakte. Cronbach's Alpha dieser Korrelationen beträgt 0.01.](img/fig_positive_action_correlation_distplot.pdf)

# Evaluationen

Teilnehmer können verschieden auf ein Artefakt reagieren. Die gesamte
Konfrontation mit dem Artefakt wird als **negative Evaluation** gewertet,
falls der Teilnehmer mit dem Artefakt interagiert, aber keine positive Aktion
wie z.B. die Benachrichtigung eines Helpdesks ausführt (**-pos ∩ neg**).
Ansonsten wird sie als **positive Evaluation** gewertet.

Alle Werte beziehen sich auf die relative Häufigkeit von Evaluationen pro
Teilnehmer.  
**μ**: Durchschnitt  
**σ**: Standardabweichung  
**min**: Minimum  
**max**: Maximum  

| Gruppe | Typ | $\mu$ | $\sigma$ | min | max |
| :----- | :-- | ----: | -------: | --: | --: |
| Alle | Positiv | 0.83 | 0.35 | 0.0 | 1.0 |
| | Negativ | 0.17 | 0.35 | 0.0 | 1.0 |
| iv | Positiv | 1.00 | 0.00 | 1.0 | 1.0 |
| | Negativ | 0.00 | 0.00 | 0.0 | 0.0 |
| -iv | Positiv | 0.62 | 0.48 | 0.0 | 1.0 |
| | Negativ | 0.38 | 0.48 | 0.0 | 1.0 |
: Evaluationen der Teilnehmer, gruppiert nach Interventionsteilnahme und Typ
der Evaluation.

![Kerndichteschätzung für Korrelationen zwischen positiven Evaluationen verschiedener Artefakte. Cronbach's Alpha dieser Korrelationen beträgt 1.00.](img/fig_good_eval_correlation_distplot.pdf)

# Ergebnisse

\## CITSA

Die *Kollektive IT-Security Awareness* (**CITSA**) ist abhängig von der Größe
der Organisation in Mitarbeitern mit Bildschirmarbeitsplatz $n$. Sie basiert
auf der Wahrscheinlichkeit, dass ein Angriff der ein Nutzer-wahrnehmbares
Artefakt bei $x\, (1 \le x \le n)$ Nutzern bedingt, erfolgreich ist. Das ist genau
dann der Fall, wenn mindestens ein Nutzer mit dem Artefakt interagiert, es
selbst nicht meldet und auch kein anderer der $x$ Nutzer es meldet:
$$
P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1} * P(x = \#artifacts)
$$
Da die Größe des Angriffs unklar ist, muss $x$ als gleichverteilt angenommen
werden und es wird $P(x = \#artifacts) = 1$ für alle $x$ gesetzt. Dies lässt
sich nun aufsummieren und mitteln:
$$
\frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
Um hieraus die Wahrscheinlichkeit für einen verhinderten Angriff zu ermitteln,
wird dieser Wert von 1 abgezogen. Somit ergibt sich die CITSA wie folgt:
$$
1 - \frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
Die CITSA liegt zwischen 0 und 1. Je höher der Wert, um so besser ist die
Kollektive IT-Security Awareness.

**iv**: Durchschnittliche CITSA von Interventionsteilnehmern.  
**-iv**: Durchschnittliche CITSA von Nicht-Interventionsteilnehmern.  
**-iv &rarr; iv**: CITSA Verbesserung zwischen Nicht-Interventionsteilnehmern
und Interventionsteilnehmern.  

| iv | -iv | **-iv &rarr; iv** |
| -: | --: | ----------------: |
| 1.0000 | 0.9999 | **0.01%** |
: CITSA der Teilnehmer, gruppiert nach Interventionsteilnahme.

\## IITSA

Die *Individuelle IT-Security Awareness* (**IITSA**) ist die mittlere
Wahrscheinlichkeit für das Gegenereignis einer Artefaktreaktion ohne Report:
$1-P(\neg pos \, \cap \, neg)$. Als Wahrscheinlichkeit liegt dieser Wert
zwischen 0 und 1. Ein höherer Wert bedeutet bessere Individuelle IT-Security
Awareness.

**iv**: Durchschnittliche IITSA von Interventionsteilnehmern.  
**-iv**: Durchschnittliche IITSA von Nicht-Interventionsteilnehmern.  
**-iv &rarr; iv**: IITSA Verbesserung zwischen Nicht-Interventionsteilnehmern
und Interventionsteilnehmern.  

| iv | -iv | **-iv &rarr; iv** |
| -: | --: | ----------------: |
| 1.0000 | 0.5714 | **75.00%** |
: IITSA der Teilnehmer, gruppiert nach Interventionsteilnahme.

![Kerndichteschätzung der durchschnittlichen IITSA getrennt nach Interventionsteilnahme.](img/fig_iitsa_kde.pdf)

# Artefakte
-  af1  
![af1](img1)
![af1](img2)
-  af2  
*Keine Screenshots verfügbar*
"""
        series_info = {
            'title': 'test_title',
            'start': 'start',
            'end': 'end',
            '#participants': '#participants',
            '#artifacts': '#artifacts'
        }
        artifacts = [
            {'recipe_id': 'af1', 'images': ['img1', 'img2']},
            {'recipe_id': 'af2', 'images': []}
        ]
        participation_stats = {
            '#artifacts': '1.70 0.67 0 2',
            '#schooled': '5 (50.00%)',
            '#armed': '9 (90.00%)',
            '#missed': '1 (10.00%)'
        }
        reaction_stats = {
            'p_nopos': '41.18%',
            'p_pos_neg': '41.18%',
            'p_pos_noneg': '17.65%',
            'p_nopos_neg': '17.65%',
            'p_nopos_noneg': '23.53%',
            'iv_p_nopos': '30.00%',
            'noiv_p_nopos': '57.14%',
            'iv_p_neg': '40.00%',
            'noiv_p_neg': '85.71%',
            'iv_p_nopos_neg': '0.00%',
            'noiv_p_nopos_neg': '42.86%',
            'iv_citsa': '1.0000',
            'noiv_citsa': '0.9999',
            'citsa_increase': '0.01%',
            'iv_iitsa': '1.0000',
            'noiv_iitsa': '0.5714',
            'iitsa_increase': '75.00%',
            'good_evals_mean': '0.83',
            'good_evals_std': '0.35',
            'good_evals_min': '0.0',
            'good_evals_max': '1.0',
            'bad_evals_mean': '0.17',
            'bad_evals_std': '0.35',
            'bad_evals_min': '0.0',
            'bad_evals_max': '1.0',
            'good_evals_iv_mean': '1.00',
            'good_evals_iv_std': '0.00',
            'good_evals_iv_min': '1.0',
            'good_evals_iv_max': '1.0',
            'bad_evals_iv_mean': '0.00',
            'bad_evals_iv_std': '0.00',
            'bad_evals_iv_min': '0.0',
            'bad_evals_iv_max': '0.0',
            'good_evals_noiv_mean': '0.62',
            'good_evals_noiv_std': '0.48',
            'good_evals_noiv_min': '0.0',
            'good_evals_noiv_max': '1.0',
            'bad_evals_noiv_mean': '0.38',
            'bad_evals_noiv_std': '0.48',
            'bad_evals_noiv_min': '0.0',
            'bad_evals_noiv_max': '1.0'
        }
        tokens = {
            'series_info': series_info,
            'participation_stats': participation_stats,
            'reaction_stats': reaction_stats,
            'artifacts': artifacts,
            'date': '1970-01-01',
            'author': 'Super Author',
            'cronbachs_alpha': {'good_eval': '1.00', 'positive_action': '0.01'}
        }
        lookup = TemplateLookup(directories=[TEMPLATE_PATH])
        tpl = lookup.get_template(LABELED_NAME_DE)
        output = tpl.render(**tokens)
        assert output == expected_output


    def test_pre_post_template(self):
        expected_output = r"""
---
title: 'Report der Serien "pre_title" und "post_title"'
author: 'Super Author'
mainfont: TeX Gyre Pagella
papersize: a4
date: 1970-01-01
lang: de-DE
---

# Studiendesign

Die Studie fand in zwei verschiedenen Phasen statt. In der ersten Phase wurden
**#participants1 Teilnehmern** zwischen dem
**start1** und dem **end1**
nacheinander jeweils **#artifacts1 Artefakte**
zugespielt, und relevante Aktionen im Bezug auf das jeweilige Artefakt
aufgezeichnet.  
Die Teilnehmer erhielten in der ersten Phase die folgenden Artefakte:

-  af1
-  af2

Anschließend wurde eine Intervention mit einem Teil der Teilnehmer durchgeführt
(für Details siehe Abschnitt "Teilnahmen").

Danach gab es eine zweite Phase, in welcher
**#participants2 Teilnehmern** zwischen dem
**start2** und dem **end2** jeweils
**#artifacts2 Artefakte** zugespielt wurden. Hier wurden
ebenfalls die relevanten Aktionen im Bezug auf das jeweilige Artefakt
aufgezeichnet.  
In der zweiten Phase erhielten die Teilnehmer folgende Artefakte:

-  af3
-  af4

# Teilnahme

- Anzahl der Teilnehmer in Phase 1: **10**
- Anzahl der Teilnehmer in Phase 2: **9**
- Phase 1 ∩ Phase 2: **8**
- Phase 1 \\ Phase 2 (%): **2 (20.00%)**
- Phase 2 \\ Phase 1 (%): **1 (11.11%)**
- Anzahl der Interventionsteilnehmer (%): **4 (36.36%)**
- Anzahl der Interventionsteilnehmer in Phase 1 (%): **4 (40.00%)**
- Anzahl der Interventionsteilnehmer in Phase 2 (%): **4 (44.44%)**
- Anzahl der Interventionsteilnehmer in (Phase 1 ∩ Phase 2) (%): **4 (50.00%)**
- Anzahl der nicht gearmten Teilnehmer in Phase 1 (%): **1 (10.00%)**
- Anzahl der gearmten Teilnehmer in Phase 1 (%): **9 (90.00%)**
- Anzahl der Artefakte pro Teilnehmer in Phase 1 (avg, std, min, max): **1.70 0.67 0 2**
- Anzahl der nicht gearmten Teilnehmer in Phase 2 (%): **2 (22.22%)**
- Anzahl der gearmten Teilnehmer in Phase 2 (%): **7 (77.78%)**
- Anzahl der Artefakte pro Teilnehmer in Phase 2 (avg, std, min, max): **1.44 0.88 0 2**

# Reaktionen

Alle Werte der Tabellen 1 bis 3 geben die durchschnittliche relative Häufigkeit
für die folgenden Ereignisse an:  
**-pos**: keine positive Reaktion  
**neg**: negative Reaktion  
**pos ∩ neg**: sowohl positive, als auch negative Reaktion  
**pos ∩ -neg**: positive Reaktion, aber keine negative Reaktion  
**-pos ∩ neg**: keine positive Reaktion, aber negative Reaktion  
**-pos ∩ -neg**: weder positive, noch negative Reaktion  

| Phase | -pos | pos ∩ neg | pos ∩ -neg | -pos ∩ neg | -pos ∩ -neg |
| :---- | ---: | --------: | ---------: | ---------: | ----------: |
| Phase 1 | 41.18% | 47.06% | 11.76% | 23.53% | 17.65% |
| Phase 2 | 61.54% | 38.46% | 0.00% | 30.77% | 30.77% |
| Phase 1 ∪ Phase 2 | 50.00% | 43.33% | 6.67% | 26.67% | 23.33% |
: Reaktionen aller Teilnehmer.

| Phase | -pos | neg | -pos ∩ neg |
| :---- | ---: | --: | ---------: |
| Phase 1 | 12.50% | 87.50% | 12.50% |
| Phase 2 | 50.00% | 75.00% | 25.00% |
| Phase 1 ∪ Phase 2 | 31.25% | 81.25% | 18.75% |
: Reaktionen von Interventionsteilnehmern.

| Phase | -pos | neg | -pos ∩ neg |
| :---- | ---: | --: | ---------: |
| Phase 1 | 66.67% | 55.56% | 33.33% |
| Phase 2 | 80.00% | 60.00% | 40.00% |
| Phase 1 ∪ Phase 2 | 71.43% | 57.14% | 35.71% |
: Reaktionen von Nicht-Interventionsteilnehmern.

![Nutzer Aktionen als Reaktion auf Artefakte.](img/fig_reactions_by_artifact_relative.pdf)

![Kerndichteschätzung der Korrelationen zwischen positiven Reaktionen verschiedener Artefakte. Cronbach's Alpha dieser Korrelationen beträgt 0.01.](img/fig_positive_action_correlation_distplot.pdf)

# Evaluationen

Teilnehmer können verschieden auf ein Artefakt reagieren. Die gesamte
Konfrontation mit dem Artefakt wird als **negative Evaluation** gewertet,
falls der Teilnehmer mit dem Artefakt interagiert, aber keine positive Aktion
wie z.B. die Benachrichtigung eines Helpdesks ausführt (**neg ∩ -pos**).
Ansonsten wird sie als **positive Evaluation** gewertet.

Alle Werte beziehen sich auf die relative Häufigkeit von Evaluationen pro
Teilnehmer.  
**μ**: Durchschnitt  
**σ**: Standardabweichung  
**min**: Minimum  
**max**: Maximum

| Phase | Typ | Gruppe | $\mu$ | $\sigma$ | min | max |
| :---- | --: | -----: | ----: | -------: | --: | --: |
| Phase 1 | Positiv | Alle | 0.78 | 0.36 | 0.0 | 1.0 |
| | | iv | 0.88 | 0.25 | 0.5 | 1.0 |
| | | -iv | 0.70 | 0.45 | 0.0 | 1.0 |
| | Negativ | Alle | 0.22 | 0.36 | 0.0 | 1.0 |
| | | iv | 0.12 | 0.25 | 0.0 | 0.5 |
| | | -iv | 0.30 | 0.45 | 0.0 | 1.0 |
| Phase 2 | Positiv | Alle | 0.71 | 0.39 | 0.0 | 1.0 |
| | | iv | 0.75 | 0.29 | 0.5 | 1.0 |
| | | -iv | 0.67 | 0.58 | 0.0 | 1.0 |
| | Negativ | Alle | 0.29 | 0.39 | 0.0 | 1.0 |
| | | iv | 0.25 | 0.29 | 0.0 | 0.5 |
| | | -iv | 0.33 | 0.58 | 0.0 | 1.0 |
| Phase 1 ∪ Phase 2 | Positiv | Alle | 0.78 | 0.28 | 0.25 | 1.0 |
| | | iv | 0.81 | 0.12 | 0.75 | 1.0 |
| | | -iv | 0.76 | 0.37 | 0.25 | 1.0 |
| | Negativ | Alle | 0.22 | 0.28 | 0.0 | 0.75 |
| | | iv | 0.19 | 0.12 | 0.0 | 0.25 |
| | | -iv | 0.24 | 0.37 | 0.0 | 0.75 |
: Evaluationen der Teilnehmer, gruppiert nach Studienphase, Typ der Evaluation
und Interventionsteilnahme.

![Kerndichteschätzung für Korrelationen zwischen positiven Evaluationen verschiedener Artefakte. Cronbach's Alpha dieser Korrelationen beträgt 1.00.](img/fig_good_eval_correlation_distplot.pdf)

# Ergebnisse

\## CITSA

Die *Kollektive IT-Security Awareness* (**CITSA**) ist abhängig von der Größe
der Organisation in Mitarbeitern mit Bildschirmarbeitsplatz $n$. Sie basiert
auf der Wahrscheinlichkeit, dass ein Angriff der ein Nutzer-wahrnehmbares
Artefakt bei $x\, (1 \le x \le n)$ Nutzern bedingt, erfolgreich ist. Das ist genau
dann der Fall, wenn mindestens ein Nutzer mit dem Artefakt interagiert, es
selbst nicht meldet und auch kein anderer der $x$ Nutzer es meldet:
$$
P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1} * P(x = \#artifacts)
$$
Da die Größe des Angriffs unklar ist, muss $x$ als gleichverteilt angenommen
werden und es wird $P(x = \#artifacts) = 1$ für alle $x$ gesetzt. Dies lässt
sich nun aufsummieren und mitteln:
$$
\frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
Um hieraus die Wahrscheinlichkeit für einen verhinderten Angriff zu ermitteln,
wird dieser Wert von 1 abgezogen. Somit ergibt sich die CITSA wie folgt:
$$
1 - \frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
Die CITSA liegt zwischen 0 und 1. Je höher der Wert, um so besser ist die
Kollektive IT-Security Awareness.

**iv**: Durchschnittliche CITSA von Interventionsteilnehmern.  
**-iv**: Durchschnittliche CITSA von Nicht-Interventionsteilnehmern.  
**-iv &rarr; iv**: CITSA Verbesserung zwischen Nicht-Interventionsteilnehmern
und Interventionsteilnehmern.  

| Phase | iv | -iv | **-iv &rarr; iv** |
| :---- | -: | --: | ------------: |
| Phase 1 | 1.0000 | 0.9999 | **0.01%** |
| Phase 2 | 1.0000 | 0.9998 | **0.01%** |
| Phase 1 ∪ Phase 2 | 1.0000 | 0.9999 | **0.01%** |
: CITSA der Teilnehmer, gruppiert nach Studienphase und Interventionsteilnahme.

\## IITSA

Die *Individuelle IT-Security Awareness* (**IITSA**) ist die mittlere
Wahrscheinlichkeit für das Gegenereignis einer Artefaktreaktion ohne Report:
$1-P(\neg pos \, \cap \, neg)$. Als Wahrscheinlichkeit liegt dieser Wert
zwischen 0 und 1. Ein höherer Wert bedeutet bessere Individuelle IT-Security
Awareness.

**iv**: Durchschnittliche IITSA von Interventionsteilnehmern.  
**-iv**: Durchschnittliche IITSA von Nicht-Interventionsteilnehmern.  
**-iv &rarr; iv**: IITSA Verbesserung zwischen Nicht-Interventionsteilnehmern
und Interventionsteilnehmern.  

| Phase | iv | -iv | **-iv &rarr; iv** |
| :---- | -: | --: | ------------: |
| Phase 1 | 0.8750 | 0.6667 | **31.25%** |
| Phase 2 | 0.7500 | 0.6000 | **25.00%** |
| Phase 1 ∪ Phase 2 | 0.8125 | 0.6429 | **26.39%** |
: IITSA der Teilnehmer, gruppiert nach Studienphase und Interventionsteilnahme.

![Kerndichteschätzung der durchschnittlichen IITSA für Artefakte der ersten Phase, getrennt nach
Interventionsteilnahme.](img/fig_phase_1_iitsa_kde.pdf)

![Kerndichteschätzung der durchschnittlichen IITSA für Artefakte der zweiten Phase, getrennt nach
Interventionsteilnahme.](img/fig_phase_2_iitsa_kde.pdf)

# Artefakte

-  af1  
![af1](img1)
![af1](img2)
-  af2  
*Keine Screenshots verfügbar*
-  af3  
*Keine Screenshots verfügbar*
-  af4  
![af4](img3)
![af4](img4)
"""

        series_pre_info = {
            'title': 'pre_title',
            'start': 'start1',
            'end': 'end1',
            '#participants': '#participants1',
            '#artifacts': '#artifacts1'
        }
        series_post_info = {
            'title': 'post_title',
            'start': 'start2',
            'end': 'end2',
            '#participants': '#participants2',
            '#artifacts': '#artifacts2'
        }
        artifacts_pre = [
            {'recipe_id': 'af1', 'images': ['img1', 'img2']},
            {'recipe_id': 'af2', 'images': []}
        ]
        artifacts_post = [
            {'recipe_id': 'af3', 'images': []},
            {'recipe_id': 'af4', 'images': ['img3', 'img4']}
        ]
        participation_stats = {
            '#pre_participants': 10,
            '#post_participants': 9,
            '#pre_x_post': 8,
            '#pre_only': '2 (20.00%)',
            '#post_only': '1 (11.11%)',
            '#schooled': '4 (36.36%)',
            '#schooled_pre': '4 (40.00%)',
            '#schooled_post': '4 (44.44%)',
            '#schooled_pre_post': '4 (50.00%)',
            '#missed_pre': '1 (10.00%)',
            '#armed_pre': '9 (90.00%)',
            '#pre_artifacts': '1.70 0.67 0 2',
            '#missed_post': '2 (22.22%)',
            '#armed_post': '7 (77.78%)',
            '#post_artifacts': '1.44 0.88 0 2'
        }
        reaction_stats = {
            'p_nopos_pre': '41.18%',
            'p_nopos_post': '61.54%',
            'p_nopos': '50.00%',
            'p_pos_neg_pre': '47.06%',
            'p_pos_neg_post': '38.46%',
            'p_pos_neg': '43.33%',
            'p_pos_noneg_pre': '11.76%',
            'p_pos_noneg_post': '0.00%',
            'p_pos_noneg': '6.67%',
            'p_nopos_neg_pre': '23.53%',
            'p_nopos_neg_post': '30.77%',
            'p_nopos_neg': '26.67%',
            'p_nopos_noneg_pre': '17.65%',
            'p_nopos_noneg_post': '30.77%',
            'p_nopos_noneg': '23.33%',
            'iv_p_nopos_pre': '12.50%',
            'iv_p_nopos_post': '50.00%',
            'iv_p_nopos': '31.25%',
            'noiv_p_nopos_pre': '66.67%',
            'noiv_p_nopos_post': '80.00%',
            'noiv_p_nopos': '71.43%',
            'iv_p_neg_pre': '87.50%',
            'iv_p_neg_post': '75.00%',
            'iv_p_neg': '81.25%',
            'noiv_p_neg_pre': '55.56%',
            'noiv_p_neg_post': '60.00%',
            'noiv_p_neg': '57.14%',
            'iv_p_nopos_neg_pre': '12.50%',
            'iv_p_nopos_neg_post': '25.00%',
            'iv_p_nopos_neg': '18.75%',
            'noiv_p_nopos_neg_pre': '33.33%',
            'noiv_p_nopos_neg_post': '40.00%',
            'noiv_p_nopos_neg': '35.71%',
            'iv_citsa_pre': '1.0000',
            'iv_citsa_post': '1.0000',
            'iv_citsa': '1.0000',
            'noiv_citsa_pre': '0.9999',
            'noiv_citsa_post': '0.9998',
            'noiv_citsa': '0.9999',
            'citsa_increase_pre': '0.01%',
            'citsa_increase_post': '0.01%',
            'citsa_increase': '0.01%',
            'iv_iitsa_pre': '0.8750',
            'iv_iitsa_post': '0.7500',
            'iv_iitsa': '0.8125',
            'noiv_iitsa_pre': '0.6667',
            'noiv_iitsa_post': '0.6000',
            'noiv_iitsa': '0.6429',
            'iitsa_increase_pre': '31.25%',
            'iitsa_increase_post': '25.00%',
            'iitsa_increase': '26.39%',
            'good_evals_pre_mean': '0.78',
            'good_evals_pre_std': '0.36',
            'good_evals_pre_min': '0.0',
            'good_evals_pre_max': '1.0',
            'good_evals_post_mean': '0.71',
            'good_evals_post_std': '0.39',
            'good_evals_post_min': '0.0',
            'good_evals_post_max': '1.0',
            'good_evals_mean': '0.78',
            'good_evals_std': '0.28',
            'good_evals_min': '0.25',
            'good_evals_max': '1.0',
            'bad_evals_pre_mean': '0.22',
            'bad_evals_pre_std': '0.36',
            'bad_evals_pre_min': '0.0',
            'bad_evals_pre_max': '1.0',
            'bad_evals_post_mean': '0.29',
            'bad_evals_post_std': '0.39',
            'bad_evals_post_min': '0.0',
            'bad_evals_post_max': '1.0',
            'bad_evals_mean': '0.22',
            'bad_evals_std': '0.28',
            'bad_evals_min': '0.0',
            'bad_evals_max': '0.75',
            'good_evals_iv_pre_mean': '0.88',
            'good_evals_iv_pre_std': '0.25',
            'good_evals_iv_pre_min': '0.5',
            'good_evals_iv_pre_max': '1.0',
            'good_evals_iv_post_mean': '0.75',
            'good_evals_iv_post_std': '0.29',
            'good_evals_iv_post_min': '0.5',
            'good_evals_iv_post_max': '1.0',
            'good_evals_iv_mean': '0.81',
            'good_evals_iv_std': '0.12',
            'good_evals_iv_min': '0.75',
            'good_evals_iv_max': '1.0',
            'bad_evals_iv_pre_mean': '0.12',
            'bad_evals_iv_pre_std': '0.25',
            'bad_evals_iv_pre_min': '0.0',
            'bad_evals_iv_pre_max': '0.5',
            'bad_evals_iv_post_mean': '0.25',
            'bad_evals_iv_post_std': '0.29',
            'bad_evals_iv_post_min': '0.0',
            'bad_evals_iv_post_max': '0.5',
            'bad_evals_iv_mean': '0.19',
            'bad_evals_iv_std': '0.12',
            'bad_evals_iv_min': '0.0',
            'bad_evals_iv_max': '0.25',
            'good_evals_noiv_pre_mean': '0.70',
            'good_evals_noiv_pre_std': '0.45',
            'good_evals_noiv_pre_min': '0.0',
            'good_evals_noiv_pre_max': '1.0',
            'good_evals_noiv_post_mean': '0.67',
            'good_evals_noiv_post_std': '0.58',
            'good_evals_noiv_post_min': '0.0',
            'good_evals_noiv_post_max': '1.0',
            'good_evals_noiv_mean': '0.76',
            'good_evals_noiv_std': '0.37',
            'good_evals_noiv_min': '0.25',
            'good_evals_noiv_max': '1.0',
            'bad_evals_noiv_pre_mean': '0.30',
            'bad_evals_noiv_pre_std': '0.45',
            'bad_evals_noiv_pre_min': '0.0',
            'bad_evals_noiv_pre_max': '1.0',
            'bad_evals_noiv_post_mean': '0.33',
            'bad_evals_noiv_post_std': '0.58',
            'bad_evals_noiv_post_min': '0.0',
            'bad_evals_noiv_post_max': '1.0',
            'bad_evals_noiv_mean': '0.24',
            'bad_evals_noiv_std': '0.37',
            'bad_evals_noiv_min': '0.0',
            'bad_evals_noiv_max': '0.75'
        }
        tokens = {
            'series_pre_info': series_pre_info,
            'series_post_info': series_post_info,
            'artifacts_pre': artifacts_pre,
            'participation_stats': participation_stats,
            'reaction_stats': reaction_stats,
            'artifacts_post': artifacts_post,
            'date': '1970-01-01',
            'author': 'Super Author',
            'cronbachs_alpha': {'good_eval': '1.00', 'positive_action': '0.01'}
        }
        lookup = TemplateLookup(directories=[TEMPLATE_PATH])
        tpl = lookup.get_template(PRE_POST_NAME_DE)
        output = tpl.render(**tokens)
        assert output == expected_output

class TestEnglish:
    def test_labeled_template(self):
        expected_output = r"""
---
title: 'Report for series "test_title"'
author: 'Super Author'
mainfont: TeX Gyre Pagella
papersize: a4
date: 1970-01-01
lang: en-US
---

# Study design

In this study, **#participants participants** were
presented with **#artifacts artifacts** between
**start** and **end**, and the relevant
actions regarding each artifact were recorded. Some participants have
participated in an intervention beforehand.

# Participation

- Number of artifacts (avg, std, min, max): **1.70 0.67 0 2**
- Number of intervention participants (%): **5 (50.00%)**
- Number of armed participants (%): **9 (90.00%)**
- Number of non-armed participants (%): **1 (10.00%)**

# Reactions

All values in the tables 1 to 3 state the average relative frequency for the
following events:  
**-pos**: no positive reaction  
**pos ∩ neg**: positive and negative reaction  
**pos ∩ -neg**: positive, but no negative reaction  
**-pos ∩ neg**: no positive, but negative reaction  
**-pos ∩ -neg**: neither positive, nor negative reaction  

| -pos | pos ∩ neg | pos ∩ -neg | -pos ∩ neg | -pos ∩ -neg |
| ---: | --------: | ---------: | ---------: | ----------: |
| 41.18% | 41.18% | 17.65% | 17.65% | 23.53% |
: Reactions of all participants.

| -pos | neg | -pos ∩ neg |
| ---: | --: | ---------: |
| 30.00% | 40.00% | 0.00% |
: Reactions of intervention participants.

| -pos | neg | -pos ∩ neg |
| ---: | --: | ---------: |
| 57.14% | 85.71% | 42.86% |
: Reactions of non-intervention participants.

![User actions in reaction to artifacts.](img/fig_reactions_by_artifact_relative.pdf)

![Kernel density estimation for correlations between positive actions of different artifacts. Cronbach's alpha for these correlations amounts to 0.01.](img/fig_positive_action_correlation_distplot.pdf)

# Evaluations

Participants can have different reactions to different artifacts. The overall
confrontation with the artifact will be considered a **negative evaluation**,
if the participant interacted with the artifact, but did not perform a positive
action like calling the helpdesk (**neg ∩ -pos**). Otherwise, it is considered
a **positive evaluation**.

All values refer to the relative frequencies of evaluations per participant.  
**μ**: Average  
**σ**: Standard deviation  
**min**: Minimum  
**max**: Maximum

| Group | Type | $\mu$ | $\sigma$ | min | max |
| :----- | :-- | ----: | -------: | --: | --: |
| all | positive | 0.83 | 0.35 | 0.0 | 1.0 |
| | negative | 0.17 | 0.35 | 0.0 | 1.0 |
| iv | positive | 1.00 | 0.00 | 1.0 | 1.0 |
| | negative | 0.00 | 0.00 | 0.0 | 0.0 |
| -iv | positive | 0.62 | 0.48 | 0.0 | 1.0 |
| | negative | 0.38 | 0.48 | 0.0 | 1.0 |
: Evaluations of participants, grouped by intervention participation and type
of evaluation.

![Kernel density estimation for the correlation between positive evaluations for different artifacts. Cronbach's alpha for these correlations amounts to 1.00.](img/fig_good_eval_correlation_distplot.pdf)

# Results

\## CITSA

The *Collective IT-Security Awareness* (**CITSA**) is depending on the size of
the organization in employees with a VDU workstation $n$. It is based on the
probability, that an attack which depends on a user perceptible artifact is
successful for $x \, (1 \le x \le n)$ users. This is the case if and only if at
least one user interacts with the artifact, does not report it, and none of the
other $x$ users report it either:
$$
P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1} * P(x = \#artifacts)
$$
Since the size of the attack is unknown, we have to assume $x$ as uniformly
distributed. Thus, we assume $P(x = \#artifacts) = 1$ for all $x$. Now, we can
sum everything up and take the average:
$$
\frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
To determine the probability of a prevented attack, we subtract this value
from 1. Thus, the CITSA results as follows:
$$
1 - \frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
The CITSA ranges from 0 to 1. The higher the value, the better is the
Collective IT-Security Awareness.

**iv**: Average CITSA of intervention participants  
**-iv**: Average CITSA of non-intervention participants  
**-iv &rarr; iv**: CITSA increase from non-intervention participants to
intervention participants.  

| iv | -iv | **-iv &rarr; iv** |
| -: | --: | ------------: |
| 1.0000 | 0.9999 | **0.01%** |
: CITSA of participants, grouped by intervention participation.

\## IITSA

The *Individual IT-Security Awareness* (**IITSA**) is the average probability
of the complementary event of an artifact reaction without a report:
$1-P(\neg pos \, \cap \, neg)$. As a probability, its value ranges from 0 to 1.
The higher the value, the better the Individual IT-Security Awareness.

**iv**: Average IITSA of intervention participants  
**-iv**: Average IITSA of non-intervention participants  
**-iv &rarr; iv**: IITSA increase from non-intervention participants to
intervention participants.  

| iv | -iv | **-iv &rarr; iv** |
| -: | --: | ------------: |
| 1.0000 | 0.5714 | **75.00%** |
: IITSA of participants, grouped by intervention participation.

![Kernel density estimation for average individual IT security awareness separated by intervention
participation.](img/fig_iitsa_kde.pdf)

# Artifacts
-  af1  
![af1](img1)
![af1](img2)
-  af2  
*No screenshots available*
"""
        series_info = {
            'title': 'test_title',
            'start': 'start',
            'end': 'end',
            '#participants': '#participants',
            '#artifacts': '#artifacts'
        }
        artifacts = [
            {'recipe_id': 'af1', 'images': ['img1', 'img2']},
            {'recipe_id': 'af2', 'images': []}
        ]
        participation_stats = {
            '#artifacts': '1.70 0.67 0 2',
            '#schooled': '5 (50.00%)',
            '#armed': '9 (90.00%)',
            '#missed': '1 (10.00%)'
        }
        reaction_stats = {
            'p_nopos': '41.18%',
            'p_pos_neg': '41.18%',
            'p_pos_noneg': '17.65%',
            'p_nopos_neg': '17.65%',
            'p_nopos_noneg': '23.53%',
            'iv_p_nopos': '30.00%',
            'noiv_p_nopos': '57.14%',
            'iv_p_neg': '40.00%',
            'noiv_p_neg': '85.71%',
            'iv_p_nopos_neg': '0.00%',
            'noiv_p_nopos_neg': '42.86%',
            'iv_citsa': '1.0000',
            'noiv_citsa': '0.9999',
            'citsa_increase': '0.01%',
            'iv_iitsa': '1.0000',
            'noiv_iitsa': '0.5714',
            'iitsa_increase': '75.00%',
            'good_evals_mean': '0.83',
            'good_evals_std': '0.35',
            'good_evals_min': '0.0',
            'good_evals_max': '1.0',
            'bad_evals_mean': '0.17',
            'bad_evals_std': '0.35',
            'bad_evals_min': '0.0',
            'bad_evals_max': '1.0',
            'good_evals_iv_mean': '1.00',
            'good_evals_iv_std': '0.00',
            'good_evals_iv_min': '1.0',
            'good_evals_iv_max': '1.0',
            'bad_evals_iv_mean': '0.00',
            'bad_evals_iv_std': '0.00',
            'bad_evals_iv_min': '0.0',
            'bad_evals_iv_max': '0.0',
            'good_evals_noiv_mean': '0.62',
            'good_evals_noiv_std': '0.48',
            'good_evals_noiv_min': '0.0',
            'good_evals_noiv_max': '1.0',
            'bad_evals_noiv_mean': '0.38',
            'bad_evals_noiv_std': '0.48',
            'bad_evals_noiv_min': '0.0',
            'bad_evals_noiv_max': '1.0'
        }
        tokens = {
            'series_info': series_info,
            'participation_stats': participation_stats,
            'reaction_stats': reaction_stats,
            'artifacts': artifacts,
            'date': '1970-01-01',
            'author': 'Super Author',
            'cronbachs_alpha': {'good_eval': '1.00', 'positive_action': '0.01'}
        }
        lookup = TemplateLookup(directories=[TEMPLATE_PATH])
        tpl = lookup.get_template(LABELED_NAME_EN)
        output = tpl.render(**tokens)
        assert output == expected_output


    def test_pre_post_template(self):
        expected_output = r"""
---
title: 'Report for series "pre_title" and "post_title"'
author: 'Super Author'
mainfont: TeX Gyre Pagella
papersize: a4
date: 1970-01-01
lang: en-US
---

# Study design

The study was conducted in two different phases. In the first phase,
**#participants1 participants** were presented with
**#artifacts1 artifacts** between
**start1** and **end1**.
Relevant actions related to each artifact were recorded.  
In the first phase, the participants received the following artifacts:

-  af1
-  af2

Afterwards, an Intervention was performed with some of the participants (for
details refer to the "Participation" section).

Then a second phase was realized, in which
**#participants2 participants** were presented with
**#artifacts2 artifacts** between
**start2** and **end2**.
Relevant actions related to each artifact were recorded.  
In the second phase, the participants received the following artifacts:

-  af3
-  af4

# Participation

- Number of phase 1 participants: **10**
- Number of phase 2 participants: **9**
- Phase 1 ∩ Phase 2: **8**
- Phase 1 \\ Phase 2 (%): **2 (20.00%)**
- Phase 2 \\ Phase 1 (%): **1 (11.11%)**
- Number of intervention participants (%): **4 (36.36%)**
- Number of intervention participants in phase 1 (%): **4 (40.00%)**
- Number of intervention participants in phase 2 (%): **4 (44.44%)**
- Number of intervention participants in (phase 1 ∩ phase 2) (%): **4 (50.00%)**
- Number of non-armed participants in phase 1 (%): **1 (10.00%)**
- Number of armed participants in phase 1 (%): **9 (90.00%)**
- Number of artifacts per participant in phase 1 (avg, std, min, max): **1.70 0.67 0 2**
- Number of non-armed participants in phase 2 (%): **2 (22.22%)**
- Number of armed participants in phase 2 (%): **7 (77.78%)**
- Number of artifacts per participant in phase 2 (avg, std, min, max): **1.44 0.88 0 2**

# Reactions

All values in the tables 1 to 3 state the average relative frequency for the
following events:  
**-pos**: no positive reaction  
**pos ∩ neg**: positive and negative reaction  
**pos ∩ -neg**: positive, but no negative reaction  
**-pos ∩ neg**: no positive, but negative reaction  
**-pos ∩ -neg**: neither positive, nor negative reaction  

| Phase | -pos | pos ∩ neg | pos ∩ -neg | -pos ∩ neg | -pos ∩ -neg |
| :---- | ---: | --------: | ---------: | ---------: | ----------: |
| Phase 1 | 41.18% | 47.06% | 11.76% | 23.53% | 17.65% |
| Phase 2 | 61.54% | 38.46% | 0.00% | 30.77% | 30.77% |
| Phase 1 ∪ Phase 2 | 50.00% | 43.33% | 6.67% | 26.67% | 23.33% |
: Reactions of all participants.

| Phase | -pos | neg | -pos ∩ neg |
| :---- | ---: | --: | ---------: |
| Phase 1 | 12.50% | 87.50% | 12.50% |
| Phase 2 | 50.00% | 75.00% | 25.00% |
| Phase 1 ∪ Phase 2 | 31.25% | 81.25% | 18.75% |
: Reactions of intervention participants.

| Phase | -pos | neg | -pos ∩ neg |
| :---- | ---: | --: | ---------: |
| Phase 1 | 66.67% | 55.56% | 33.33% |
| Phase 2 | 80.00% | 60.00% | 40.00% |
| Phase 1 ∪ Phase 2 | 71.43% | 57.14% | 35.71% |
: Reactions of non-intervention participants.

![User actions in reaction to artifacts.](img/fig_reactions_by_artifact_relative.pdf)

![Kernel density estimation for correlations between positive actions of different artifacts. Cronbach's alpha for these correlations amounts to 0.01.](img/fig_positive_action_correlation_distplot.pdf)

# Evaluations

Participants can have different reactions to different artifacts. The overall
confrontation with the artifact will be considered a **negative evaluation**,
if the participant interacted with the artifact, but did not perform a positive
action like calling the helpdesk (**neg ∩ -pos**). Otherwise, it is considered
a **positive evaluation**.

All values refer to the relative frequencies of evaluations per participant.  
**μ**: Average  
**σ**: Standard deviation  
**min**: Minimum  
**max**: Maximum

| Phase | Type | Group | $\mu$ | $\sigma$ | min | max |
| :---- | :--- | :---- | ----: | -------: | --: | --: |
| Phase 1 | positive | all | 0.78 | 0.36 | 0.0 | 1.0 |
| | | iv | 0.88 | 0.25 | 0.5 | 1.0 |
| | | -iv | 0.70 | 0.45 | 0.0 | 1.0 |
| | negative | all | 0.22 | 0.36 | 0.0 | 1.0 |
| | | iv | 0.12 | 0.25 | 0.0 | 0.5 |
| | | -iv | 0.30 | 0.45 | 0.0 | 1.0 |
| Phase 2 | positive | all | 0.71 | 0.39 | 0.0 | 1.0 |
| | | iv | 0.75 | 0.29 | 0.5 | 1.0 |
| | | -iv | 0.67 | 0.58 | 0.0 | 1.0 |
| | negative | all | 0.29 | 0.39 | 0.0 | 1.0 |
| | | iv | 0.25 | 0.29 | 0.0 | 0.5 |
| | | -iv | 0.33 | 0.58 | 0.0 | 1.0 |
| Phase 1 ∪ Phase 2 | positive | all | 0.78 | 0.28 | 0.25 | 1.0 |
| | | iv | 0.81 | 0.12 | 0.75 | 1.0 |
| | | -iv | 0.76 | 0.37 | 0.25 | 1.0 |
| | negative | all | 0.22 | 0.28 | 0.0 | 0.75 |
| | | iv | 0.19 | 0.12 | 0.0 | 0.25 |
| | | -iv | 0.24 | 0.37 | 0.0 | 0.75 |
: Evaluations of participants, grouped by study phase, type of evaluation and
intervention participation.

![Kernel density estimation for the correlation between positive evaluations for different artifacts. Cronbach's alpha for these correlations amounts to 1.00.](img/fig_good_eval_correlation_distplot.pdf)

# Results

\## CITSA

The *Collective IT-Security Awareness* (**CITSA**) is depending on the size of
the organization in employees with a VDU workstation $n$. It is based on the
probability, that an attack which depends on a user perceptible artifact is
successful for $x \, (1 \le x \le n)$ users. This is the case if and only if at
least one user interacts with the artifact, does not report it, and none of the
other $x$ users report it either:
$$
P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1} * P(x = \#artifacts)
$$
Since the size of the attack is unknown, we have to assume $x$ as uniformly
distributed. Thus, we assume $P(x = \#artifacts) = 1$ for all $x$. Now, we can
sum everything up and take the average:
$$
\frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
To determine the probability of a prevented attack, we subtract this value
from 1. Thus, the CITSA results as follows:
$$
1 - \frac{1}{n} * \sum_{x=1}^{n} P(\neg pos \, \cap \, neg) * P(\neg pos)^{x-1}
$$
The CITSA ranges from 0 to 1. The higher the value, the better is the
Collective IT-Security Awareness.

**iv**: Average CITSA of intervention participants  
**-iv**: Average CITSA of non-intervention participants  
**-iv &rarr; iv**: CITSA increase from non-intervention participants to
intervention participants.  

| Phase | iv | -iv | **-iv &rarr; iv** |
| :---- | -: | --: | ------------: |
| Phase 1 | 1.0000 | 0.9999 | **0.01%** |
| Phase 2 | 1.0000 | 0.9998 | **0.01%** |
| Phase 1 ∪ Phase 2 | 1.0000 | 0.9999 | **0.01%** |
: CITSA of participants, grouped by study phase and intervention participation.

\## IITSA

The *Individual IT-Security Awareness* (**IITSA**) is the average probability
of the complementary event of an artifact reaction without a report:
$1-P(\neg pos \, \cap \, neg)$. As a probability, its value ranges from 0 to 1.
The higher the value, the better the Individual IT-Security Awareness.

**iv**: Average IITSA of intervention participants  
**-iv**: Average IITSA of non-intervention participants  
**-iv &rarr; iv**: IITSA increase from non-intervention participants to
intervention participants.  

| Phase | iv | -iv | **-iv &rarr; iv** |
| :---- | -: | --: | ------------: |
| Phase 1 | 0.8750 | 0.6667 | **31.25%** |
| Phase 2 | 0.7500 | 0.6000 | **25.00%** |
| Phase 1 ∪ Phase 2 | 0.8125 | 0.6429 | **26.39%** |
: IITSA of participants, grouped by study phase and intervention participation.

![Kernel density estimation for average individual IT security awareness on artifacts of Phase 1
separated by intervention participation.](img/fig_phase_1_iitsa_kde.pdf)

![Kernel density estimation for average individual IT security awareness on artifacts of Phase 2
separated by intervention participation.](img/fig_phase_2_iitsa_kde.pdf)

# Artifacts

-  af1  
![af1](img1)
![af1](img2)
-  af2  
*No screenshots available*
-  af3  
*No screenshots available*
-  af4  
![af4](img3)
![af4](img4)
"""

        series_pre_info = {
            'title': 'pre_title',
            'start': 'start1',
            'end': 'end1',
            '#participants': '#participants1',
            '#artifacts': '#artifacts1'
        }
        series_post_info = {
            'title': 'post_title',
            'start': 'start2',
            'end': 'end2',
            '#participants': '#participants2',
            '#artifacts': '#artifacts2'
        }
        artifacts_pre = [
            {'recipe_id': 'af1', 'images': ['img1', 'img2']},
            {'recipe_id': 'af2', 'images': []}
        ]
        artifacts_post = [
            {'recipe_id': 'af3', 'images': []},
            {'recipe_id': 'af4', 'images': ['img3', 'img4']}
        ]
        participation_stats = {
            '#pre_participants': 10,
            '#post_participants': 9,
            '#pre_x_post': 8,
            '#pre_only': '2 (20.00%)',
            '#post_only': '1 (11.11%)',
            '#schooled': '4 (36.36%)',
            '#schooled_pre': '4 (40.00%)',
            '#schooled_post': '4 (44.44%)',
            '#schooled_pre_post': '4 (50.00%)',
            '#missed_pre': '1 (10.00%)',
            '#armed_pre': '9 (90.00%)',
            '#pre_artifacts': '1.70 0.67 0 2',
            '#missed_post': '2 (22.22%)',
            '#armed_post': '7 (77.78%)',
            '#post_artifacts': '1.44 0.88 0 2'
        }
        reaction_stats = {
            'p_nopos_pre': '41.18%',
            'p_nopos_post': '61.54%',
            'p_nopos': '50.00%',
            'p_pos_neg_pre': '47.06%',
            'p_pos_neg_post': '38.46%',
            'p_pos_neg': '43.33%',
            'p_pos_noneg_pre': '11.76%',
            'p_pos_noneg_post': '0.00%',
            'p_pos_noneg': '6.67%',
            'p_nopos_neg_pre': '23.53%',
            'p_nopos_neg_post': '30.77%',
            'p_nopos_neg': '26.67%',
            'p_nopos_noneg_pre': '17.65%',
            'p_nopos_noneg_post': '30.77%',
            'p_nopos_noneg': '23.33%',
            'iv_p_nopos_pre': '12.50%',
            'iv_p_nopos_post': '50.00%',
            'iv_p_nopos': '31.25%',
            'noiv_p_nopos_pre': '66.67%',
            'noiv_p_nopos_post': '80.00%',
            'noiv_p_nopos': '71.43%',
            'iv_p_neg_pre': '87.50%',
            'iv_p_neg_post': '75.00%',
            'iv_p_neg': '81.25%',
            'noiv_p_neg_pre': '55.56%',
            'noiv_p_neg_post': '60.00%',
            'noiv_p_neg': '57.14%',
            'iv_p_nopos_neg_pre': '12.50%',
            'iv_p_nopos_neg_post': '25.00%',
            'iv_p_nopos_neg': '18.75%',
            'noiv_p_nopos_neg_pre': '33.33%',
            'noiv_p_nopos_neg_post': '40.00%',
            'noiv_p_nopos_neg': '35.71%',
            'iv_citsa_pre': '1.0000',
            'iv_citsa_post': '1.0000',
            'iv_citsa': '1.0000',
            'noiv_citsa_pre': '0.9999',
            'noiv_citsa_post': '0.9998',
            'noiv_citsa': '0.9999',
            'citsa_increase_pre': '0.01%',
            'citsa_increase_post': '0.01%',
            'citsa_increase': '0.01%',
            'iv_iitsa_pre': '0.8750',
            'iv_iitsa_post': '0.7500',
            'iv_iitsa': '0.8125',
            'noiv_iitsa_pre': '0.6667',
            'noiv_iitsa_post': '0.6000',
            'noiv_iitsa': '0.6429',
            'iitsa_increase_pre': '31.25%',
            'iitsa_increase_post': '25.00%',
            'iitsa_increase': '26.39%',
            'good_evals_pre_mean': '0.78',
            'good_evals_pre_std': '0.36',
            'good_evals_pre_min': '0.0',
            'good_evals_pre_max': '1.0',
            'good_evals_post_mean': '0.71',
            'good_evals_post_std': '0.39',
            'good_evals_post_min': '0.0',
            'good_evals_post_max': '1.0',
            'good_evals_mean': '0.78',
            'good_evals_std': '0.28',
            'good_evals_min': '0.25',
            'good_evals_max': '1.0',
            'bad_evals_pre_mean': '0.22',
            'bad_evals_pre_std': '0.36',
            'bad_evals_pre_min': '0.0',
            'bad_evals_pre_max': '1.0',
            'bad_evals_post_mean': '0.29',
            'bad_evals_post_std': '0.39',
            'bad_evals_post_min': '0.0',
            'bad_evals_post_max': '1.0',
            'bad_evals_mean': '0.22',
            'bad_evals_std': '0.28',
            'bad_evals_min': '0.0',
            'bad_evals_max': '0.75',
            'good_evals_iv_pre_mean': '0.88',
            'good_evals_iv_pre_std': '0.25',
            'good_evals_iv_pre_min': '0.5',
            'good_evals_iv_pre_max': '1.0',
            'good_evals_iv_post_mean': '0.75',
            'good_evals_iv_post_std': '0.29',
            'good_evals_iv_post_min': '0.5',
            'good_evals_iv_post_max': '1.0',
            'good_evals_iv_mean': '0.81',
            'good_evals_iv_std': '0.12',
            'good_evals_iv_min': '0.75',
            'good_evals_iv_max': '1.0',
            'bad_evals_iv_pre_mean': '0.12',
            'bad_evals_iv_pre_std': '0.25',
            'bad_evals_iv_pre_min': '0.0',
            'bad_evals_iv_pre_max': '0.5',
            'bad_evals_iv_post_mean': '0.25',
            'bad_evals_iv_post_std': '0.29',
            'bad_evals_iv_post_min': '0.0',
            'bad_evals_iv_post_max': '0.5',
            'bad_evals_iv_mean': '0.19',
            'bad_evals_iv_std': '0.12',
            'bad_evals_iv_min': '0.0',
            'bad_evals_iv_max': '0.25',
            'good_evals_noiv_pre_mean': '0.70',
            'good_evals_noiv_pre_std': '0.45',
            'good_evals_noiv_pre_min': '0.0',
            'good_evals_noiv_pre_max': '1.0',
            'good_evals_noiv_post_mean': '0.67',
            'good_evals_noiv_post_std': '0.58',
            'good_evals_noiv_post_min': '0.0',
            'good_evals_noiv_post_max': '1.0',
            'good_evals_noiv_mean': '0.76',
            'good_evals_noiv_std': '0.37',
            'good_evals_noiv_min': '0.25',
            'good_evals_noiv_max': '1.0',
            'bad_evals_noiv_pre_mean': '0.30',
            'bad_evals_noiv_pre_std': '0.45',
            'bad_evals_noiv_pre_min': '0.0',
            'bad_evals_noiv_pre_max': '1.0',
            'bad_evals_noiv_post_mean': '0.33',
            'bad_evals_noiv_post_std': '0.58',
            'bad_evals_noiv_post_min': '0.0',
            'bad_evals_noiv_post_max': '1.0',
            'bad_evals_noiv_mean': '0.24',
            'bad_evals_noiv_std': '0.37',
            'bad_evals_noiv_min': '0.0',
            'bad_evals_noiv_max': '0.75'
        }
        tokens = {
            'series_pre_info': series_pre_info,
            'series_post_info': series_post_info,
            'artifacts_pre': artifacts_pre,
            'participation_stats': participation_stats,
            'reaction_stats': reaction_stats,
            'artifacts_post': artifacts_post,
            'date': '1970-01-01',
            'author': 'Super Author',
            'cronbachs_alpha': {'good_eval': '1.00', 'positive_action': '0.01'}
        }
        lookup = TemplateLookup(directories=[TEMPLATE_PATH])
        tpl = lookup.get_template(PRE_POST_NAME_EN)
        output = tpl.render(**tokens)
        assert output == expected_output
